Source: purpose
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake (>= 3.0~),
               debhelper (>= 11),
               extra-cmake-modules (>= 5.51.0~),
               intltool,
               kaccounts-integration,
               kirigami2-dev,
               libaccounts-glib-dev,
               libaccounts-qt5-dev,
               libkaccounts-dev,
               libkf5config-dev (>= 5.51.0~),
               libkf5coreaddons-dev (>= 5.51.0~),
               libkf5declarative-dev,
               libkf5i18n-dev (>= 5.51.0~),
               libkf5kcmutils-dev,
               libkf5kio-dev (>= 5.51.0~),
               libkf5notifications-dev,
               libkf5wallet-dev,
               libsignon-qt5-dev,
               pkg-config,
               pkg-kde-tools (>= 0.15.15ubuntu1~),
               qtbase5-dev (>= 5.8.0~),
               qtdeclarative5-dev (>= 5.8.0~)
Standards-Version: 4.1.4
Homepage: https://cgit.kde.org/purpose.git/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/purpose
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/purpose.git

Package: libkf5purpose-bin
Architecture: any
Multi-Arch: same
Depends: qml-module-org-kde-bluezqt,
         qml-module-org-kde-kirigami2,
         qml-module-org-kde-kquickcontrolsaddons (>= 5.51),
         qml-module-qtquick-dialogs,
         qml-module-ubuntu-onlineaccounts,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: kdeconnect, nodejs
Breaks: kamoso (<< 3.2.1~), libkf5purpose5 (<< 5.53.0)
Replaces: kamoso (<< 3.2.1~), libkf5purpose5 (<< 5.53.0)
Description: abstraction to provide and leverage actions of a specific kind, runtime
 Purpose offers the possibility to create integrate services and actions on
 any application without having to implement them specifically. Purpose will
 offer them mechanisms to list the different alternatives to execute given the
 requested action type and will facilitate components so that all the plugins
 can receive all the information they need.
 .
 This package contains the Purpose runtime elements.

Package: libkf5purpose-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libkf5coreaddons-dev (>= 5.51.0~),
         libkf5purpose-bin (= ${binary:Version}),
         libkf5purpose5 (= ${binary:Version}),
         qtbase5-dev (>= 5.8.0~),
         ${misc:Depends}
Description: abstraction to provide and leverage actions of a specific kind, devel files
 Purpose offers the possibility to create integrate services and actions on
 any application without having to implement them specifically. Purpose will
 offer them mechanisms to list the different alternatives to execute given the
 requested action type and will facilitate components so that all the plugins
 can receive all the information they need.
 .
 This package contains the Purpose development files.

Package: libkf5purpose5
Architecture: any
Multi-Arch: same
Depends: qml-module-org-kde-kquickcontrolsaddons (>= 5.51),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: libkf5purpose-bin (<< 5.53.0)
Conflicts: libkf5purposewidgets5 (<< 1.1-2~)
Replaces: libkf5purpose-bin (<< 5.53.0), libkf5purposewidgets5 (<< 1.1-2~)
Recommends: libkf5purpose-bin, qml-module-org-kde-purpose (= ${binary:Version})
Suggests: kde-telepathy-send-file
Description: library for abstractions to get the developer's purposes fulfilled
 Framework for providing abstractions to get the developer's purposes fulfilled.
 .
 This package contains the Purpose library.

Package: qml-module-org-kde-purpose
Architecture: any
Multi-Arch: same
Replaces: qml-modules-org-kde-purpose (<< 1.1-5~)
Breaks: qml-modules-org-kde-purpose (<< 1.1-5~)
Depends: qml-module-qtquick-controls,
         qml-module-qtquick-layouts,
         qml-module-qtquick2,
         qml-module-ubuntu-onlineaccounts,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: kdeconnect
Description: abstraction to provide and leverage actions of a specific kind, qml bindings
 Purpose offers the possibility to create integrate services and actions on
 any application without having to implement them specifically. Purpose will
 offer them mechanisms to list the different alternatives to execute given the
 requested action type and will facilitate components so that all the plugins
 can receive all the information they need.
 .
 This package contains the Purpose Qt Quick QML bindings.
